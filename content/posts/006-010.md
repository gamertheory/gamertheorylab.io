+++
title = "006-010"
lastmod = 2021-01-28T10:25:58-03:00
categories = ["AGONIA en la caverna TM"]
draft = false
summary = "Esto es lo que observas del mundo fuera de La Caverna: La totalidad de la vida aparece como una vasta acumulación de mercancías y espectáculos, de cosas envueltas en imágenes e imágenes vendidas como cosas"
date = "2021-01-28 10:00:00"
+++

Fichas: [006](#006) - [007](#007)- [008](#008) - [009](#009)

## 006 {#006}

<div style="float:left;margin:2px 20px 2px 2px; text-align: justify;">
  <div></div>

<a href="http://www.futureofthebook.org/gamertheory2.0/index.html@p=7.html" target="_blank"><img src="/images/gamerTheory006.png" alt="Imagen y link a la tarjeta seis" style="float:left;margin:2px 20px 2px 2px; text-align: justify;" width="300; text-align: right" /></a>
ESTO ES lo que observas del mundo fuera de La Caverna: La totalidad de la vida aparece como una vasta acumulación de mercancías y espectáculos, de cosas envueltas en imágenes e imágenes vendidas como cosas[^fn:1]. ¿Pero cómo se organizan esas cosas e imágenes y que rol nos piden que adoptemos todes y cada une de nosotres hacia ellas? Las imágenes nos atraen como si fueran premios, y nos llaman a jugar el juego en el que son lo único que está en juego. Observas que mundo tras mundo, cueva tras cueva, lo que prevalece es el mismo agon, la misma lógica digital de uno contra el otro, para terminar en victoria o derrota ¡La agonía gobierna! Todas las cosas tiene valor solo cuando se las compara con otras, todo el mundo tiene valor solo cuando es comparado con otre. Todas las situaciones son ganar-perder, a menos que sean ganar-ganar — una situación en donde les jugadores son libres de colaborar solo porque buscan sus premios en diferentes juegos. El mundo real aparece como un gran salón de videojuegos dividido en muchos y variados juegos. El trabajo como una carrera de locos[^fn:2]. La política una carrera de caballos. La economía un casino. Incluso la utópica justicia que vendrá en la otra vida está excluida: aquel que muere como más juguetes gana. Los juegos ya no son un pasatiempo, fuera o dentro de nuestra vida. Ahora son la forma misma de la vida, de la muerte y el tiempo mismo. Estos juegos no son broma. Cuando la pantalla muestra game over, estás muerto, o derrotado o, en el mejor de los casos expulsado.
</div>

## 007 {#007}

<div style="float:left;margin:2px 20px 2px 2px; text-align: justify;">
  <div></div>

<a href="http://www.futureofthebook.org/gamertheory2.0/index.html@p=8.html" target="_blank"><img src="/images/gamerTheory007.png" alt="Imagen y link a la tarjeta ocho" style="float:left;margin:2px 20px 2px 2px; text-align: justify;" width="300; text-align: right" /></a>
EL JUEGO ha colonizado a sus rivales dentro del ámbito cultural, desde el espectáculo del cine hasta las simulaciones de la televisón. Las historias ya no nos sedan con soluciones imaginarias a los problemas reales. Las historias solo cuentan los pasos por los cuales alguien venció a otro — una gran victoria para quienes apuestan a lo imaginario.[^fn:3] El único género televisivo original de los inicios del siglo 21 no se llama "Reality Show" por nada. Brenton & Cohen: "Al firmar las autorizaciones legales, los concursantes acuerdan terminar siendo como estadísticas, manipulando cada sentimiento y acción... lo que conduce a infidelidades, lagrimas y quizás corazones rotos"[^fn:4]. Está claro, los Reality Shows no parecen la realidad, pero tampoco la realidad. Ambos parecen juegos. Ambos devienen espacios continuos, donde los jugadores prueban sus habilidades en escenarios forzados. Las situaciones pueden ser artificiales, el diálogo menos que espontáneo y los jugadores pueden simplemente estar haciendo lo que los productores les dicen. Todo esto es una pieza perfecta con una realidad que es en sí misma una arena artificial, donde todo el mundo ya es un jugador, esperando su turno.
</div>

## 008 {#008}

<div style="float:left;margin:2px 20px 2px 2px; text-align: justify;">
  <div></div>

<a href="http://www.futureofthebook.org/gamertheory2.0/index.html@p=9.html" target="_blank"><img src="/images/gamerTheory008.png" alt="Imagen y link a la tarjeta ocho" style="float:left;margin:2px 20px 2px 2px; text-align: justify;" width="300; text-align: right" /></a>
EL JUEGO, no solo a colonizado la realidad, sino que también es el único ideal que queda. El Gamespace proclama su legitimidad a través de su victoria sobre todos los rivales. La ideología reinante imagina el mundo como un campo de juego en igualdad de condiciones, sobre el cual todes somos iguales ante Dios, el gran diseñador de juegos. Historia, política, cultura; gamespace dinamita todo lo que no está en el juego, como si fueran obsoletos casinos de Las Vegas. Todo es evacuado desde un espacio y tiempo vacío que ahora nos parece natural, neutral y sin cualidades —un espacio-juego. Los límites están claramente marcados. Cada acción es tan solo un medio para un fin. Todo lo que importa son los puntos. En cuanto a quién es dueño de los equipos y quién dirige el espectáculo, es mejor no preguntar. En cuanto a quién está excluido de las grandes ligas, mejor no preguntar. En cuanto a quién lleva la puntuación y quién establece las reglas, es mejor no preguntar. En cuanto a qué órgano rector pone las dificultades y sobre qué base, es mejor no preguntar. Todo es lo mejor en el mejor y único mundo posible. Existe, para ponerle un nombre, un complejo militar del entretenimiento, y gobierna. Sus triunfos afirman no solo las reglas del juego sino que regla el juego.
</div>

## 009 {#009}

<div style="float:left;margin:2px 20px 2px 2px; text-align: justify;">
  <div></div>

<a href="http://www.futureofthebook.org/gamertheory2.0/index.html@p=9.html" target="_blank"><img src="/images/gamerTheory009.png" alt="Imagen y link a la tarjeta nueve" style="float:left;margin:2px 20px 2px 2px; text-align: justify;" width="300; text-align: right" /></a>
Todo lo que el complejo militar de entretenimiento toca con sus conectores enchapados en oro se convierte en dígitos. Todo es digital y, sin embargo, lo digital no es nada. Ningún humano puede tocarlo, olerlo o saborearlo. Solo son pitidos y flashes que se muestran a sí mismo con un brillo alfanumérico, enviando cotizaciones de la bolsa a a tu teléfono celular. Sí, puede ser que tenga vívidos gráficos en 3D. Puede aparecer con gráficos de barra o de torta. Incluso polígonos espiralados y arremolinados de brillantes colores que destellan de una pantalla a otra. Pero estas son solo decoraciones. La vibración de tu dedo sobre el botón o el temblor de tu muñeca sobre el mouse se conecta directamente a un invisible, intangible espacio de juego de pura competencia, puro agon. No importa su tu cueva viene equipada con una Playstation o una terminal Bloomberg. No importa si creés que estas jugando con el mercado de bonos o con el Grand Theft Auto. Todo es tan solo un algoritmo, lo suficientemente desconocido, como para convertirlo en un juego.

---

[^fn:1]: [N.T.]Esté párrafo llevaba una llamada, pero el link a la llamada se perdió.
[^fn:2]: [N.T.]Rat race en el original. Carrera de ratas. Pero el termino no tiene una traducción con sentido al español. Se refiere a "búsqueda interminable, contraproducente o inútil. La frase equipara a los humanos con ratas que intentan ganar una recompensa como el queso, en vano. También puede referirse a una lucha competitiva para salir adelante financiera o rutinariamente. El término se asocia comúnmente con un estilo de vida agotador y repetitivo que no deja tiempo para la relajación o el disfrute." <https://es.wikipedia.org/wiki/Rat%5Frace>
[^fn:3]: Vale la pena agregar la respuesta por la pregunta a esta frase de Mackenzie Wark. <a href="https://twitter.com/amaciel/status/1356601547946414081" target="_blank"><img src="/images/response001.png" alt="Este tweet">
[^fn:4]: Sam Brenton and Reuben Cohen, Shooting People: Adventures in Reality TV (Londres: Verso, 2003), p. 57. ver también Mark Andrejevic, Reality TV: The Work of Being Watched (Lanham NC: Rowman & Littlefield, 2004). La frase se refiere a los contratos y acuerdos a los que llegan los participantes de los reality shows.
