+++
title = "info"
lastmod = 2021-01-28T08:34:09-03:00
draft = false
+++

Esta es una traducción del libro [Gamer Theory](http://www.futureofthebook.org/gamertheory2.0/) versión 2.0 de [Mckenzie Wark](https://twitter.com/mckenziewark).

El plan de trabajo aspira a traducir una ficha de cada capítulo por día. Tengan en cuenta que es una aspiración. O más bien, una expresión de deseo.

En los momentos que crea conveniente voy a usar algo de lenguaje inclusivo, y también el plural. El texto está dirigido a ustedes que están ahí. No solo a usted que está leyendo.

Mi costado nerdo me invita a seguir algunos lineamientos para abordar el trabajo, en este caso extraidos del [Zen de Python](https://es.wikipedia.org/wiki/Zen%5Fde%5FPython):

- Bello es mejor que feo.
- Explícito es mejor que implícito.
- Simple es mejor que complejo.
- Complejo es mejor que complicado.
- Plano es mejor que anidado.
- Disperso es mejor que denso.
- La legibilidad es importante.
- Los casos especiales no son lo suficientemente especiales como para romper las reglas.
- Aunque lo práctico gana a la pureza.
- Los errores nunca deberían pasar silenciosamente. A menos que se silencien explícitamente.
- Frente a la ambigüedad, rechaza la tentación de adivinar.
- Debería haber una, y preferiblemente solo una, manera obvia de hacerlo.
- Aunque esa manera puede no ser obvia al principio a menos que usted sea holandés.
- Ahora es mejor que nunca.
- Aunque nunca es a menudo mejor que ya mismo..
- Si la implementación es difícil de explicar, es una mala idea.
- Si la implementación es fácil de explicar, puede que sea una buena idea.

Si están intereades en colaborar, no duden en escribirme.
